import unittest
from check_version import check_version, check_version_distutils, santitize_base_version

class TestCheckVersion(unittest.TestCase):
    def test_check_version(self):
        self.assertTrue(check_version(santitize_base_version(">1.1"), "1.2"))
        self.assertTrue(check_version(santitize_base_version(">1.1"), "1.1.1"))
        self.assertFalse(check_version(santitize_base_version(">1.1"), "1.0"))

    def test_check_version_distutils(self):
        self.assertTrue(check_version_distutils(santitize_base_version(">1.1"), "1.2"))
        self.assertTrue(check_version_distutils(santitize_base_version(">1.1"), "1.1.1"))
        self.assertFalse(check_version_distutils(santitize_base_version(">1.1"), "1.0"))

if __name__ == '__main__':
    unittest.main()

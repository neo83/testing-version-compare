## How to Run
Couple of simple functions to demonstrate version checking. One is using a simple string compare and the other using in built function.
Second distutils function was there to compare results with the not-so intelligent string comparison with respect to lack of understanding
of what a version number is and how they are formed

### Run the code using:
`python check_version.py base_version version_to_check`

Example: `python check_version.py ">1.1" "1.1.1`

### Run tests
`python test_check_version.py`
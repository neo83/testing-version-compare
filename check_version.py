#!/usr/bin/env python

import sys
from distutils.version import StrictVersion

def santitize_base_version(base_version):
    """ return base_version without '>' character. Would usually be done in main
    but doing this in a function to run the tests easier
    """
    return base_version.split('>')[1] if '>' in base_version else base_version

def check_version(base_version, version):
    """checks version greater than base version using standard string compare"""

    if version > base_version:
        return True
    else:
        return False

def check_version_distutils(base_version, version):
    """check version numbers using distutils package"""

    if StrictVersion(version) > StrictVersion(base_version):
        return True
    else:
        return False

def main():
    try:
        base_version = sys.argv[1]
        version = sys.argv[2]
    except IndexError:
        print "Usage: {} base_version version_to_compare".format(sys.argv[0])
        sys.exit(1)

    if check_version(santitize_base_version(base_version), version):
        print "version greater"
    else:
        print "version lower"

if __name__ == "__main__":
    main()
